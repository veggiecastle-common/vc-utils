const cacheTable = {};

function init() {
  return Promise.resolve();
}

function set(key, value, { expiresIn }) {
  cacheTable[key] = {
    lastObtained: Date.now(),
    expiresIn,
    value,
  };
  return Promise.resolve();
}

function get(key) {
  const cacheInfo = cacheTable[key];
  if (!cacheInfo) {
    return undefined;
  }
  if (typeof cacheInfo.expiresIn !== 'number') {
    return cacheInfo.value;
  }
  if (Date.now() - cacheInfo.lastObtained < cacheInfo.expiresIn * 1000) {
    return cacheInfo.value;
  } else {
    delete cacheTable[key];
  }
  return undefined;
}

function del(key) {
  delete cacheTable[key];
  return Promise.resolve();
}

module.exports = {
  init,
  set,
  get,
  del,
};
