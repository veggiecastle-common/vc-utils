const inMemory = require('./in-memory');
const redis = require('./redis');

let cacheEngine = null;

function init(type, options = {}) {
  switch (type) {
    case types.IN_MEMORY:
      cacheEngine = inMemory;
      return inMemory.init(options);
    case types.REDIS:
      cacheEngine = redis;
      return redis.init(options);
    default:
      throw new Error(`Unsupported cache type ${type}.`);
  }
}

async function withCache(key, expiresIn, func, options = {}) {
  if (!cacheEngine) {
    throw new Error('Cache engine not initialized.');
  }
  if (typeof key !== 'string') {
    throw new Error('Key must be a string.');
  }
  if (typeof func !== 'function') {
    throw new Error('Invalid function provided.');
  }
  const cachedResult = await cacheEngine.get(key);
  if (cachedResult !== undefined) {
    return cachedResult;
  }
  const result = await func();
  if ((result !== null && result !== undefined) || options.cacheNil) {
    if (options.synchronousSave) {
      await cacheEngine.set(key, result, { expiresIn });
    } else {
      cacheEngine.set(key, result, { expiresIn });
    }
  }
  return result;
}

function del(key, options = {}) {
  return cacheEngine.del(key, options);
}

const types = {
  IN_MEMORY: 'IN_MEMORY',
  REDIS: 'REDIS',
};

// (async () => {
//   init('REDIS', { redisEndPoint: 'localhost:6379' });
//   const a = await withCache('1', 1000, () => Promise.resolve(2));
//   console.log(a);
//   setTimeout(async () => {
//     const b = await withCache('1', 1000, () => Promise.resolve(Date.now()));
//     console.log(b);
//   }, 900);
// })();

module.exports = {
  withCache,
  types,
  init,
  del,
};
