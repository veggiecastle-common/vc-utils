const redis = require('redis');

let redisClient;

function init(options = {}) {
  const redisUrl = `redis://${options.redisEndPoint}`;
  const redisOptions = {};
  if (options.prefix) {
    redisOptions.prefix = options.prefix;
  }
  redisClient = redis.createClient(redisUrl, redisOptions);
  return Promise.resolve();
}

function set(key, value, { expiresIn }) {
  return new Promise((resolve, reject) => {
    if (typeof expiresIn !== 'number' || expiresIn < 0) {
      redisClient.set(key, JSON.stringify(value), error => {
        if (error) {
          return reject(error);
        }
        return resolve();
      });
    }
    redisClient.set(key, JSON.stringify(value), 'EX', Math.round(expiresIn / 1000), error => {
      if (error) {
        return reject(error);
      }
      return resolve();
    });
  });
}

function get(key) {
  return new Promise((resolve, reject) => redisClient.get(key, (error, data) => {
    if (error) {
      return reject(error);
    }
    if (!data) {
      return resolve();
    }
    try {
      return resolve(JSON.parse(data));
    } catch (ex) {
      return reject(ex);
    }
  }));
}

function del(key, options = {}) {
  if (options.unlink) {
    return redisClient.unlink(key);
  } else {
    return redisClient.del(key);
  }
}

module.exports = {
  init,
  set,
  get,
  del,
};
