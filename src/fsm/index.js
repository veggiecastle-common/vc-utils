const fp = require('lodash/fp');

async function transition(fsm, state, params) {
  const prevInfo = fp.get(['stateInfos', state], fsm);
  if (!prevInfo) {
    throw new Error(`Invalid state ${state}.`);
  }

  const leaveResult = await prevInfo.onLeave({ params, data: prevInfo.data, state });
  const nextState = await prevInfo.nextState({
    params, leaveResult, data: prevInfo.data, state,
  });
  const nextInfo = fp.get(['stateInfos', nextState], fsm);
  if (fp.isEmpty(nextInfo)) {
    // consider not being able to find the next state as reaching the end
    const endResult = await fsm.onEnd({ state, params, leaveResult });
    return { leaveResult, endResult, state: null };
  }

  const enterResult = await nextInfo.onEnter({
    params,
    leaveResult,
    data: nextInfo.data,
    state: nextState,
  });
  return { leaveResult, state: nextState, enterResult };
}

function create(data = {}) {
  const stateInfos = fp.mapValues(val => {
    const nextStateFunc = fp.get('nextState', val);
    if (typeof nextStateFunc !== 'function') {
      throw new Error('Fsm state must have a nextState function.');
    }
    return {
      onEnter: async params => (val.onEnter && val.onEnter(params)),
      onLeave: async params => (val.onLeave && val.onLeave(params)),
      nextState: async params => val.nextState(params),
    };
  }, data.stateInfos);

  return {
    initialState: data.initialState,
    stateInfos,
    onStart: async params => (data.onStart && data.onStart(params)),
    onEnd: async params => (data.onEnd && data.onEnd(params)),
  };
}

async function start(fsm, params = {}) {
  const initialState = fsm.initialState;
  const initialInfo = fp.get(['stateInfos', initialState], fsm);
  if (fp.isEmpty(initialInfo)) {
    return { state: null };
  }

  const startResult = await fsm.onStart(params);
  const enterResult = await initialInfo.onEnter({
    atStart: true, startResult, params, data: initialInfo.data,
  });
  return { state: initialState, enterResult, startResult };
}

module.exports = {
  create,
  transition,
  start,
};
