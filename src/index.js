const caching = require('./caching');
const locking = require('./locking');
const fsm = require('./fsm');

function sleep(ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
}

async function withRetries(func, times, options = {}) {
  let triesLeft = times;
  let delay = options.delay;
  while (triesLeft > 0) {
    try {
      // eslint-disable-next-line
      const result = await func();
      return result;
    } catch (error) {
      if (typeof options.shouldRetry === 'function' && !options.shouldRetry(error)) {
        throw error;
      }
      triesLeft -= 1;
      if (triesLeft <= 0) {
        throw error;
      }
      if (delay) {
        // eslint-disable-next-line
        await sleep(delay);
      }
      if (options.delayScheme === 'exponential-backoff') {
        delay *= 2;
      }
    }
  }
  return null;
}

module.exports = {
  sleep,
  withRetries,
  caching,
  locking,
  fsm,
};
