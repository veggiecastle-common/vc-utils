const Warlock = require('node-redis-warlock');
const redis = require('redis');

const LOCK_CONFIG = {
  TTL: 60000,
  MAX_TRY: 200,
  WAIT_TRY: 100,
};

let warlock = null;

module.exports = {
  init,
  withLock,
};

function init(_type, options = {}) {
  const redisUrl = `redis://${options.redisEndPoint}`;
  const redisClient = redis.createClient(redisUrl);
  warlock = Warlock(redisClient);
  return Promise.resolve();
}

function withLock(key, func, options = {}) {
  if (!warlock) {
    throw new Error('Lock engine not initialized.');
  }
  if (typeof key !== 'string') {
    throw new Error('Key must be a string.');
  }
  if (typeof func !== 'function') {
    throw new Error('Invalid function provided.');
  }
  return new Promise((resolve, reject) => {
    warlock.optimistic(
      key,
      options.ttl || LOCK_CONFIG.TTL,
      options.maxTry || LOCK_CONFIG.MAX_TRY,
      options.waitTry || LOCK_CONFIG.WAIT_TRY,
      async (err, unlock) => {
        if (err) {
          return reject(err);
        }
        let unlocked = false;
        try {
          const result = await func();
          if (typeof unlock === 'function') {
            if (!unlocked) {
              unlocked = true;
              unlock();
            }
          }
          return resolve(result);
        } catch (error) {
          if (typeof unlock === 'function') {
            if (!unlocked) {
              unlocked = true;
              unlock();
            }
          }
          return reject(error);
        }
      },
    );
  });
}
